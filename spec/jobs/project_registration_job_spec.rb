# frozen_string_literal: true

describe ProjectRegistrationJob, type: :job do
  subject(:job) { described_class }

  before do
    allow(Dependabot::Projects::Registration::Service).to receive(:call)
  end

  it { is_expected.to be_retryable false }

  it "queues job in low queue" do
    expect { job.perform_later }.to enqueue_sidekiq_job.on("low")
  end

  it "runs job sync" do
    job.perform_now

    expect(Dependabot::Projects::Registration::Service).to have_received(:call)
  end
end
