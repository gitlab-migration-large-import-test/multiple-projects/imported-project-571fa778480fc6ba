# frozen_string_literal: true

class GroupUpdatesContract < Dry::Validation::Contract
  params do
    config.validate_keys = true

    required(:patterns).array(:str?)
    optional(:"excluded-patterns").array(:str?)
  end
end
