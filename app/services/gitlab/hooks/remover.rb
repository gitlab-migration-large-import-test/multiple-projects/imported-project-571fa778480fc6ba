# frozen_string_literal: true

module Gitlab
  module Hooks
    class Remover < ProjectHook
      # Update existing webhook
      #
      # @return [Integer]
      def call
        log(:info, "Removing webhook for project '#{project_name}'")
        gitlab.delete_project_hook(project_name, webhook_id)
      end
    end
  end
end
