# frozen_string_literal: true

module Dependabot
  # :reek:InstanceVariableAssumption

  # Base class for dependency updater implementations
  #
  class UpdateBase < ApplicationService
    def initialize(project_name)
      @project_name = project_name
      @ungrouped_dependencies = []
    end

    private

    delegate :configuration, to: :project, prefix: :project

    attr_reader :project_name, :ungrouped_dependencies

    # Project
    #
    # @return [Project]
    def project
      raise("Implement #{self.class}#project")
    end

    # Perform dependency grouping if groups are configured
    #
    # @return [void]
    def group_dependencies
      return @ungrouped_dependencies = dependencies unless config_entry[:groups]

      log(:warn, "Dependency groups are present in configuration file but this feature is not yet supported!")
      log(:warn, "Dry run with just checking the group rules will be performed")
      log(:info, "Processing group rules")
      dependencies.each do |dependency|
        next if add_dependency_to_group(dependency)

        ungrouped_dependencies << dependency
      end
    end

    # All project dependencies
    #
    # @return [Array<Dependabot::Dependency>]
    def dependencies
      @dependencies ||= Semaphore.synchronize do
        deps = Files::Parser.call(
          source: fetcher.source,
          dependency_files: fetcher.files,
          repo_contents_path: repo_contents_path,
          config_entry: config_entry,
          credentials: credentials
        )
        dependency_name ? deps.select { |dep| dep.name == dependency_name } : deps
      end
    end

    # Array of dependencies to update
    #
    # @return [Dependabot::Dependencies::UpdatedDependency]
    def updated_dependency(dependency)
      Dependencies::UpdateChecker.call(
        dependency: dependency,
        dependency_files: fetcher.files,
        config_entry: config_entry,
        repo_contents_path: repo_contents_path,
        credentials: credentials
      )
    end

    # Add dependency to group
    #
    # @param [Dependabot::Dependency]
    # @return [Boolean]
    def add_dependency_to_group(dependency)
      matched_rule = false

      dependency_groups&.each do |group|
        log(:debug, "  checking #{dependency.name.bright} against group #{group.name.bright} rules")
        next unless group.contains?(dependency)

        log(:debug, "  adding #{dependency.name.bright} to group #{group.name.bright}")
        matched_rule = true
        group.dependencies << dependency
      end

      matched_rule
    end

    # Get file fetcher
    #
    # @return [Dependabot::FileFetcher]
    def fetcher
      @fetcher ||= Files::Fetcher.call(
        project_name: project_name,
        config_entry: config_entry,
        credentials: credentials,
        repo_contents_path: repo_contents_path
      )
    end

    # Get cloned repository path
    #
    # @return [String]
    def repo_contents_path
      return @repo_contents_path if defined?(@repo_contents_path)

      @repo_contents_path = DependabotCoreHelper.repo_contents_path(project_name, config_entry)
    end

    # Config entry for specific package ecosystem and directory
    #
    # @param [Configuration] config
    # @return [Hash]
    def config_entry(config = project_configuration)
      @config_entry ||= config
                        .entry(package_ecosystem: package_ecosystem, directory: directory)
                        .tap { |entry| raise_missing_entry_error unless entry }
    end

    # Combined credentials
    #
    # @return [Array<Hash>]
    def credentials
      @credentials ||= [
        *Credentials.call(project.gitlab_access_token),
        *registries
      ]
    end

    # Allowed private registries
    #
    # @return [Array<Hash>]
    def registries
      @registries ||= project_configuration.allowed_registries(
        package_ecosystem: package_ecosystem,
        directory: directory
      )
    end

    # Raise config missing specific entry error
    #
    # @return [void]
    def raise_missing_entry_error
      raise("Configuration is missing entry with package-ecosystem: #{package_ecosystem}, directory: #{directory}")
    end

    # Dependency groups
    #
    # @return [Array<Dependabot::DependencyGroup>]
    def dependency_groups
      @dependency_groups ||= config_entry[:groups]&.map do |name, rules|
        Dependabot::DependencyGroup.new(name: name, rules: rules)
      end
    end
  end
end
