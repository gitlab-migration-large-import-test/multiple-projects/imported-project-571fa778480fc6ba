# frozen_string_literal: true

class JobController < ApplicationController
  before_action :authenticate_user!

  def execute
    job = Update::Job.find(params.require(:id))
    project = Project.find(job.project_id)

    UpdateRunnerJob.perform_later(
      project_name: project.name,
      package_ecosystem: job.package_ecosystem,
      directory: job.directory
    )

    redirect_back_or_to projects_path, notice: "Triggered job"
  end
end
